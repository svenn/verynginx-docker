# VeryNginx Dockerfile

A very powerful and friendly nginx based on lua-nginx-module [OpenResty](https://openresty.org) which provides WAF, Control Panel and a webbased Dashboard.

This repository contains a **Dockerfile** for [VeryNginx](https://github.com/alexazhou/VeryNginx), which can also [ soon, not yet :( ] be found at the Docker Hub.

## Information

* This Dockerfile is a continued fork from the work of [camilb](https://github.com/camilb) - [https://github.com/camilb/docker-verynginx](https://github.com/camilb/docker-verynginx)
* Based on [Alpine Linux](http://www.alpinelinux.org/)  [alpine:latest](https://hub.docker.com/r/_/alpine/).
* Using [OpenSSL 1.1.1c](https://www.openssl.org/source/) to provide ChaCha20-Poly1305 cipher + TLSv1.3 support.
* Including [Brotli compression](https://github.com/google/ngx_brotli)
* Built with [Openresty version 1.15.8.1](https://openresty.org/en/changelog-1013006.html)

## Installation

    # If you want to make modifications to the Dockerfile (additional modules, etc.)
    git clone https://gitlab.com/svenn/verynginx-docker.git


## Build

If you want to install [Extra Modules](https://openresty.org), obtain the Dockerfile (described above), add the required compile-arguments and build it.

    cd ~/verynginx-docker
    docker build --rm -t svennpw/verynginx .

## Usage

    cd ~/verynginx-docker
    docker run -d -v $PWD/conf.d:/etc/nginx/conf.d -p 80:80 -p 443:443 svennpw/verynginx

## Volumes

    //TODO!

## Test it:

http://{your-server-ip}/verynginx/index.html


Default login:


`user: verynginx`


`pass: verynginx`
